resource "aws_s3_bucket" "demo_s3_bucket" {
  bucket = "${var.demo_s3_bucket_name}"
  acl    = "private"
  force_destroy = true
  tags = merge(var.customer_tags, {
    name        = "${var.demo_s3_bucket_name}"
  })
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}
